<?php

namespace App\Http\Controllers;

use App\Http\Requests\MentionRequest;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\TwitterApi;
use App\Tweet;
use App\Mention;
use App\TwitterUser;
use Carbon\Carbon;


class TwitterPeekController extends BaseController
{
    public function __construct(TwitterApi $twitterApi)
    {
        $this->twitterApi = $twitterApi;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->input('handle')) {
            $twitterHandle = preg_replace('/@/', '', $request->input('handle'));

            $twitterUser = TwitterUser::where('handle', $twitterHandle)->firstOrFail();

            $mentions = Mention::groupBy('mentioned_user_id')
                ->select('mentioned_user_id', \DB::raw('count(*) as total'))
                ->where('user_id', $twitterUser->id)
                ->orderBy('total', 'DESC')->get();
            return view('twitterpeek', compact('mentions', 'twitterUser'));
        }
        return view('twitterpeek');
    }

    /**
     * @param MentionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getMentions(MentionRequest $request)
    {
        $twitterUser = TwitterUser::firstOrCreate([
            'handle' => $request->input('handle'),
        ]);

        if (!$twitterUser->cache_created || Carbon::create()->subHour()->gt(Carbon::parse($twitterUser->cache_created))) {
            $this->updateTwitterUserCache($twitterUser);
        }
        $this->updateTwitterUserCache($twitterUser);
        return redirect()->route('twitterpeek', ['handle' => $twitterUser->handle]);

    }

    /**
     * @param TwitterUser $twitterUser
     * @return mixed
     */
    private function updateTwitterUserCache(TwitterUser $twitterUser)
    {
        $twitterUser->cache_created = Carbon::create()->toDateTimeString();
        $twitterUser->save();

        $statuses = $this->twitterApi->getUserTimeLine($twitterUser->handle);

        if (isset($statuses['error'])) {
            return redirect()->back()->withErrors($statuses['error']);
        }

        foreach ($statuses as $status) {
            $tweet = Tweet::firstOrCreate(
                [ 'tweet_id' => $status['id'] ],
                [
                    'user_id'  => $twitterUser->id,
                    'text'             => $status['text'],
                    'tweeted_on' => Carbon::parse($status['created_at'])
                ]
            );

            if (!empty($status['entities']['user_mentions'])) {
                foreach ($status['entities']['user_mentions'] as $mention) {
                    $mentioned = TwitterUser::firstOrCreate([
                        'handle' => $mention['screen_name'],
                    ]);

                    Mention::firstOrCreate([
                        'user_id'   => $twitterUser->id,
                        'mentioned_user_id' => $mentioned->id,
                        'tweet_id'          => $tweet->id
                    ]);
                }
            }
        }
    }
}
