<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mention extends Model
{
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'tweeted_on',
    ];

    public function mentioned()
    {
        return $this->hasOne(TwitterUser::class, 'id', 'mentioned_user_id');
    }
}

