<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TwitterUser extends Model
{
    protected $fillable = [
        'handle',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'cache_created',
    ];

    public function mentions()
    {
        return $this->hasManyThrough(Tweet::class, Mention::class, 'mentioned_user_id', 'id', 'id', 'tweet_id');
    }

    public function tweets()
    {
        return $this->hasMany(Tweet::class, 'user_id');
    }

    public function user_mentions()
    {
        return $this->hasMany(Mention::class, 'user_id');
    }

}
