<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Twitter Peek') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body style="padding:20px;">
<div id="app">
    <div class="container">
        <form class="form-inline" method="post" action="findMentions">
            {{ csrf_field() }}
            <div class="form-group mx-sm-3 mb-2">
                <label for="handle" class="">@</label>
                <input type="text"
                name="handle"
                class="form-control input-lg"
                id="handle"
                placeholder="Twitter Handle"
                value="{{ (@$twitterUser ? $twitterUser->handle : '') }}"
                >
            </div>
            <button type="submit" class="btn btn-primary mb-2">Find Twitter User</button>
        </form>

        <div class="row">
            <div class="col-sm-12">
                @if(@$twitterUser)
                    <h2 class="text-center">{{$twitterUser->tweets()->count() }} Tweets Searched</h2>
                @endif
                @if (@$mentions && count($mentions))
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>Count</th>
                            <th>Recent Tweets</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($mentions as $mention)
                            <tr>
                                <th scope="row">
                                    {{ '@' . $mention->mentioned->handle }}
                                </th>
                                <td>{{ $mention->total }}</td>
                                <td>
                                    <table>
                                        @foreach($mention->mentioned->mentions()->orderBy('tweeted_on', 'DESC')->get()->take(5) as $lastFive)
                                            <tr>
                                                <td style="white-space:nowrap;vertical-align: top">{{ $lastFive->tweeted_on }}</td>
                                                <td>{{ $lastFive->text }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                @else
                    <h2>Enter a Twitter Handle to see some results.</h2>
                @endif
            </div>
        </div>
    </div>

</div>

</body>
</html>
